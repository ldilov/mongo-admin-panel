FROM node:alpine
WORKDIR /app/user

COPY package.json .
RUN npm install --production && \
        apk add python && \
        apk add g++

COPY . .
CMD ["/bin/sh", "-c", "node app.js >> server.log 2>&1"]
